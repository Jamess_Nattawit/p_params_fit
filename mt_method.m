% MATLAB
% Written by nattawit
% Last update: 26 Jan 23

clear all %#ok<CLALL>
close all
clc

%% Import dataset
% Azimuth
dataset1 = readcsv('1_Az_Point3.csv'); %readcsv

Az1 = dataset1(:,4);
Az1 = Az1{:,:};
Az1 = degpositive(Az1); %degpositive.m

El1 = dataset1(:,5);
El1 = El1{:,:};

L1 = dataset1(:,7);
L1 = L1{:,:};

% Elevation
dataset0 = readcsv('0_El_Point3.csv'); %readcsv

Az0 = dataset0(:,4);
Az0 = Az0{:,:};
Az0 = degpositive(Az0); %degpositive.m

El0 = dataset0(:,5);
El0 = El0{:,:};

L0 = dataset0(:,7);
L0 = L0{:,:};

%% LSQ adjustment by matrix method

[L, X, V, V1, V0, B1, B0, C, std_uw, std_Pi, n, n1, n0] = lsqmat(Az1, El1, Az0, El0, L1, L0); %lsqmat.m

%% Make graph
% sky coverage
figure(1)
set(figure(1),'position',[0, 0, 800, 550])
Az = [Az1; Az0];
El = [El1; El0];
sz = 50;
scatter(Az, El, sz, 'b', 'o', 'LineWidth', 1.3)
title(['Sky coverage ', num2str(n), ' points'], 'FontSize', 14, 'FontWeight', 'Normal')
xlim([0 360])
xticks(0:60:360)
ylim([0 90])
yticks(0:10:90)
xlabel('Azimuth (degree)', 'FontSize', 14)
ylabel('Elevation (degree)', 'FontSize', 14)
grid on

%%
% Azimuth-Fit-(MATRIX-METHOD)
figure(2)
set(figure(2),'position',[0, 0, 900, 550])
tiledlayout(2,1)
v  = 8;
ns = n/2;
rmsAz = sqrt((V1.'* V1) / (n1 - v));
rmsEl = sqrt((V0.'* V0) / (n0 - v));
rmsPA = sqrt((V.' * V)  / (ns - v));

% Top plot
ax1 = nexttile;
sz = 50;
scatter(ax1, Az1, L1, sz, 'm', '^', 'LineWidth', 0.6)
hold on
scatter(ax1, Az1, B1, sz, 'r', 'o', 'LineWidth', 1.3)
hold off
title(['Azimuth-Errors-Fit-(MATRIX-METHOD) RMS=', sprintf('%.2f', rmsAz), ' arcsec'], 'FontSize', 14, 'FontWeight', 'Normal')
%xlim([0 360])
%xticks(0:60:360)
%ylim([-1000 0])
%yticks(-1000:200:0)
xlabel('Azimuth (degree)', 'FontSize', 14)
ylabel('delta (arcsec)', 'FontSize', 14)
grid on
legend({'actual','model'},'Location','southeast')

% Bottom plot
ax2 = nexttile;
sz = 50;
scatter(ax2, El1, L1, sz, 'm', '^', 'LineWidth', 0.6)
hold on
scatter(ax2, El1, B1, sz, 'r', 'o', 'LineWidth', 1.3)
hold off
xlim([0 90])
%xticks(0:10:90)
%xlim([10 70])
%xticks(0:10:70)
%ylim([-1000 0])
%yticks(-1000:200:0)
xlabel('Elevation (degree)', 'FontSize', 14)
ylabel('delta (arcsec)', 'FontSize', 14)
grid on
legend({'actual','model'},'Location','southeast')

%%
% Elevation-Fit-(MATRIX-METHOD)
figure(3)
set(figure(3),'position',[0, 0, 900, 550])
tiledlayout(2,1)

% Top plot
ax1 = nexttile;
sz = 50;
scatter(ax1, Az0, L0, sz, 'm', '^', 'LineWidth', 0.6)
hold on
scatter(ax1, Az0, B0, sz, 'r', 'o', 'LineWidth', 1.3)
hold off
title(['Elevation-Errors-Fit-(MATRIX-METHOD) RMS=', sprintf('%.2f', rmsEl), ' arcsec'], 'FontSize', 14, 'FontWeight', 'Normal')
%xlim([0 360])
%xticks(0:60:360)
%ylim([300 700])
%yticks(300:100:700)
xlabel('Azimuth (degree)', 'FontSize', 14)
ylabel('delta (arcsec)', 'FontSize', 14)
grid on
legend({'actual','model'},'Location','southeast')

% Bottom plot
ax2 = nexttile;
sz = 50;
scatter(ax2, El0, L0, sz, 'm', '^', 'LineWidth', 0.6)
hold on
scatter(ax2, El0, B0, sz, 'r', 'o', 'LineWidth', 1.3)
hold off
%xlim([0 90])
%xticks(0:10:90)
%xlim([10 70])
%xticks(0:10:70)
%ylim([300 700])
%yticks(300:100:700)
xlabel('Elevation (degree)', 'FontSize', 14)
ylabel('delta (arcsec)', 'FontSize', 14)
grid on
legend({'actual','model'},'Location','southeast')

%% Residual-Plot-(MATRIX-METHOD)
figure(4)
set(figure(4),'position',[0, 0, 900, 550])
tiledlayout(2,1)

% Top plot
ax1 = nexttile;
sz = 50;
scatter(ax1, Az1, V1, sz, 'b', 'o', 'LineWidth', 1.3)
title(['Residual-Plot-(MATRIX-METHOD) RMS=', sprintf('%.2f', rmsPA), ' arcsec'], 'FontSize', 14, 'FontWeight', 'Normal')
%xlim([0 360])
%xticks(0:60:360)
%ylim([-300 300])
%yticks(-300:100:300)
xlabel('Azimuth (degree)', 'FontSize', 14)
ylabel('residual (arcsec)', 'FontSize', 14)
grid on

% Bottom plot
ax2 = nexttile;
sz = 50;
scatter(ax2, El0, V0, sz, 'b', 'o', 'LineWidth', 1.3)
%xlim([0 90])
%xticks(0:10:90)
%xlim([10 70])
%xticks(0:10:70)
%ylim([-300 300])
%yticks(-300:100:300)
xlabel('Elevation (degree)', 'FontSize', 14)
ylabel('residual (arcsec)', 'FontSize', 14)
grid on

%%
%{
% Fitting errors distribution
figure(4)
set(figure(4),'position',[0, 0, 800, 550])
sz = 50;
scatter(V1, V0, sz, 'b', 'x', 'LineWidth', 0.6)
hold on
theta = linspace(0,2*pi);
r = 132;
x = r*cos(theta);
y = r*sin(theta);
plot(x, y, 'k', 'LineWidth', 0.8)
axis equal
hold off
title('Fitting errors distribution', 'FontSize', 14, 'FontWeight', 'Normal')
%xlim([-300 300])
%xticks(-300:100:300)
%ylim([-300 300])
%yticks(-300:100:300)
xlabel(['residual Az (degree) RMS=', sprintf('%.2f', rmsPA), ' arcsec'], 'FontSize', 14)
ylabel('residual El (degree)', 'FontSize', 14)
legend({'raw residual', '132 arcsec'},'Location','southeast')
grid on
%}

