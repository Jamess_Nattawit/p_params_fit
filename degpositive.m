function M = degpositive(array)
% DEGPOSITIVE Change all deg to positive
%   class(array) == double
%   class(M) == double

M = array;
for c = 1:length(M)
    if M(c) < 0
        M(c) = M(c) + 360;
    else
        M(c) = M(c);
    end
end

end
