## Description

MATLAB to solve p-parameters (P<sub>i</sub>) for TNRT based on the pointing theory of an astronomical telescope, a single-dish altazimuth mounted radio antenna. This theory analyzes fundamental instrumental alignment errors as follow:

|      Types of error         |    Parameter    |     dAz(Az, El)   |     dEl(Az El)     |
|:----------------------------|:----------------|------------------:|-------------------:|
| Zero-offset Az-encoder      | *P<sub>1</sub>* |   cos*El*         |   0                |
| Collimation error           | *P<sub>2</sub>* |   1               |   0                |
| Inclination El-axis         | *P<sub>3</sub>* |   sin*El*         |   0                |
| Inclination Az-axis N-S     | *P<sub>4</sub>* | - cos*Az* sin*El* |   sin*Az*          |
| Inclination Az-axis E-W     | *P<sub>5</sub>* |   sin*Az* sin*El* |   cos*Az*          |
| Declination error of source | *P<sub>6</sub>* |   sin*Az*         |   cos*Az* sin*El*  |
| Zero-offset El-encoder      | *P<sub>7</sub>* |   0               |   1                |
| Gravitational bending       | *P<sub>8</sub>* |   0               |   cos*El*          |
| Gravitational bending       | *P<sub>9</sub>* |   0               |   sin*El*          |  

_____________________

Construct the Pointing Equations (ignore: *P<sub>6</sub>*) 

dAz = P<sub>1</sub>cos*El* + P<sub>2</sub> + P<sub>3</sub>sin*El* - P<sub>4</sub>cos*Az*sin*El* + P<sub>5</sub>sin*Az*sin*El*

dEl = P<sub>4</sub>sin*Az* + P<sub>5</sub>cos*Az* + P<sub>7</sub> + P<sub>8</sub>cos*El* + P<sub>9</sub>sin*El*

_____________________

Determine p-parameters (P<sub>i</sub>), least-squares method is used, which the data to take into calculation consists of following set.  

{{dAz<sub>i</sub> Az<sub>i</sub> El<sub>i</sub>}, {dEl<sub>j</sub> Az<sub>j</sub> El<sub>j</sub>}} | i = 1, 2, 3, ... & j = 1, 2, 3, ...  

Least-squares is adapted to MATRIX FORM that any group of observation equations can be represented in the form as:  

*Observation equation* : AX = L + V  

*Normal equation* : A<sup>T</sup>AX = A<sup>T</sup>L  

A is the matrix of coefficients for the unknowns  
X is the matrix of unknowns  
L is the matrix of observations  
V is the matrix of residuals  

<img src="https://latex.codecogs.com/svg.image?A&space;=&space;\begin{bmatrix}&space;a_{11}&space;&space;&a_{12}&space;&space;&...&space;&space;&a_{1n}&space;\\&space;a_{21}&space;&space;&a_{22}&space;&space;&...&space;&space;&a_{2n}&space;\\&space;...&space;&space;&space;&space;&space;&...&space;&space;&space;&space;&space;&...&space;&space;&...&space;&space;&space;&space;\\&space;a_{m1}&space;&space;&a_{m2}&space;&space;&...&space;&space;&a_{mn}&space;\\\end{bmatrix}X&space;=&space;\begin{bmatrix}&space;x_{1}&space;\\&space;x_{2}&space;\\&space;...&space;&space;&space;\\&space;x_{n}&space;\\\end{bmatrix}L&space;=&space;\begin{bmatrix}&space;l_{1}&space;\\&space;l_{2}&space;\\&space;...&space;&space;&space;\\&space;l_{m}&space;\\\end{bmatrix}&space;V&space;=&space;\begin{bmatrix}&space;v_{1}&space;\\&space;v_{2}&space;\\&space;...&space;&space;&space;\\&space;v_{m}&space;\\\end{bmatrix}&space;" title="https://latex.codecogs.com/svg.image?A = \begin{bmatrix} &a_{11} &a_{12} &... &a_{1n} \\ &a_{21} &a_{22} &... &a_{2n} \\ &... &... &... &... \\ &a_{m1} &a_{m2} &... &a_{mn}\ \\end{bmatrix}X = \begin{bmatrix} x_{1} \\ x_{2} \\ ... \\ x_{n} \\\end{bmatrix}L = \begin{bmatrix} l_{1} \\ l_{2} \\ ... \\ l_{m} \\\end{bmatrix} V = \begin{bmatrix} v_{1} \\ v_{2} \\ ... \\ v_{m} \\\end{bmatrix} " /> <br>

<img src="https://latex.codecogs.com/svg.image?A&space;=&space;\begin{bmatrix}&space;cos(El_{1})&space;&space;&1&space;&space;&sin(El_{1})&space;&space;&-cos(Az_{1})sin(El_{1})&space;&space;&sin(Az_{1})sin(El_{1})&space;&space;&0&space;&space;&0&space;&space;&0&space;&space;\\&space;cos(El_{2})&space;&space;&1&space;&space;&sin(El_{2})&space;&space;&-cos(Az_{1})sin(El_{2})&space;&space;&sin(Az_{2})sin(El_{2})&space;&space;&0&space;&space;&0&space;&space;&0&space;&space;\\&space;...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;\\&space;0&space;&space;&0&space;&space;&0&space;&space;&sin(Az_{1})&space;&space;&cos(Az_{1})&space;&space;&1&space;&space;&sin(El_{1})&space;&space;&cos(El_{1})&space;&space;\\&space;0&space;&space;&0&space;&space;&0&space;&space;&sin(Az_{2})&space;&space;&cos(Az_{2})&space;&space;&1&space;&space;&sin(El_{2})&space;&space;&cos(El_{2})&space;&space;\\&space;...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;&...&space;&space;\\\end{bmatrix}" title="https://latex.codecogs.com/svg.image?A = \begin{bmatrix} &cos(El_{1}) &1 &sin(El_{1}) &-cos(Az_{1})sin(El_{1}) &sin(Az_{1})sin(El_{1}) &0 &0 &0 \\ &cos(El_{2}) &1 &sin(El_{2}) &-cos(Az_{1})sin(El_{2}) &sin(Az_{2})sin(El_{2}) &0 &0 &0 \\ &... &... &... &... &... &... &... &... \\ &0 &0 &0 &sin(Az_{1}) &cos(Az_{1}) &1 &sin(El_{1}) &cos(El_{1}) \\ &0 &0 &0 &sin(Az_{2}) &cos(Az_{2}) &1 &sin(El_{2}) &cos(El_{2}) \\ &... &... &... &... &... &... &... &... \\\end{bmatrix}" /> <br>

<img src="https://latex.codecogs.com/svg.image?X&space;=&space;\begin{bmatrix}&space;P_{1}&space;\\&space;P_{2}&space;\\&space;P_{3}&space;\\&space;P_{4}&space;\\&space;P_{5}&space;\\&space;P_{7}&space;\\&space;P_{8}&space;\\&space;P_{9}&space;\\\end{bmatrix}L&space;=&space;\begin{bmatrix}&space;dAz_{1}&space;&space;\\&space;dAz_{2}&space;&space;\\&space;...&space;&space;\\&space;...&space;&space;\\&space;dEl_{1}&space;&space;\\&space;dEl_{2}&space;&space;\\&space;...&space;&space;\\&space;...&space;&space;\\\end{bmatrix}V&space;=&space;\begin{bmatrix}&space;v_{1}&space;&space;\\&space;v_{2}&space;&space;\\&space;...&space;&space;\\&space;...&space;&space;\\\end{bmatrix}&space;" title="https://latex.codecogs.com/svg.image?X = \begin{bmatrix} &P_{1} \\ &P_{2} \\ &P_{3} \\ &P_{4} \\ &P_{5} \\ &P_{7} \\ &P_{8} \\ &P_{9} \\\end{bmatrix}L = \begin{bmatrix} &dAz_{1} \\ &dAz_{2} \\ &... \\ &... \\ &dEl_{1} \\ &dEl_{2} \\ &... \\ &... \\\end{bmatrix}V = \begin{bmatrix} &v_{1} \\ &v_{2} \\ &... \\ &... \\\end{bmatrix} " />

_____________________

*The matrix equation for calculating residuals after adjustment*  

V = AX - L

_____________________

*The standard deviation of unit weight* (<img src="https://latex.codecogs.com/svg.image?\sigma&space;_{0}" title="https://latex.codecogs.com/svg.image?\sigma _{0}" />)  

<img src="https://latex.codecogs.com/svg.image?\sigma&space;_{0}&space;=&space;\sqrt{\frac{V^{T}V}{r}}" title="https://latex.codecogs.com/svg.image?\sigma _{0} = \sqrt{\frac{V^{T}V}{r}}" />

<img src="https://latex.codecogs.com/svg.image?r" title="https://latex.codecogs.com/svg.image?r" /> is the number of degree of freedom in an adjustment, which usually equals the number of observation minus the number of unknowns, or <img src="https://latex.codecogs.com/svg.image?r&space;=&space;m&space;-&space;n" title="https://latex.codecogs.com/svg.image?s = \frac{m}{2} - n" /> <br />  

_____________________

*The standard deviation of the adjusted quantities* (<img src="https://latex.codecogs.com/svg.image?\sigma&space;_{x_{i}}" title="https://latex.codecogs.com/svg.image?\sigma _{x_{i}}" />)  

<img src="https://latex.codecogs.com/svg.image?\sigma&space;_{x_{i}}&space;=&space;\sigma&space;_{0}\sqrt{q_{x_{i},&space;x_{i}}}" title="https://latex.codecogs.com/svg.image?\sigma _{x_{i}} = \sigma _{0}\sqrt{q_{x_{i}, x_{i}}}" />  

<img src="https://latex.codecogs.com/svg.image?q_{x_{i},&space;x_{i}}" title="https://latex.codecogs.com/svg.image?q_{x_{i}, x_{i}}" /> is the diagonal element in the x<sup>th</sup> row and x<sup>th</sup> column of matrix (A<sup>T</sup>A)<sup>-1</sup>. The matrix (A<sup>T</sup>A)<sup>-1</sup> is the so-called cofactor matrix, and symbolized hereon by <img src="https://latex.codecogs.com/svg.image?Q_{xx}" title="https://latex.codecogs.com/svg.image?Q_{xx}" /> , and <img src="https://latex.codecogs.com/svg.image?\sigma&space;_{0}^{2}Q_{xx}" title="https://latex.codecogs.com/svg.image?\sigma _{0}^{2}Q_{xx}" /> is the covariance matrix of the adjustment.  

_____________________

*The pointing accuracy of the telescope after adjustment* (<img src="https://latex.codecogs.com/svg.image?P.A." title="https://latex.codecogs.com/svg.image?P.A." />)  

<img src="https://latex.codecogs.com/svg.image?P.A._{az}=\sqrt{\frac{V_{az}^{T}V_{az}}{r_{az}}}" title="https://latex.codecogs.com/svg.image?P.A._{az}=\sqrt{\frac{V_{az}^{T}V_{az}}{r_{az}}}" />   
<br>
<img src="https://latex.codecogs.com/svg.image?P.A._{el}=\sqrt{\frac{V_{el}^{T}V_{el}}{r_{el}}}" title="https://latex.codecogs.com/svg.image?P.A._{el}=\sqrt{\frac{V_{el}^{T}V_{el}}{r_{el}}}" />
<br>
<img src="https://latex.codecogs.com/svg.image?P.A.=\sqrt{\frac{V^{T}V}{s}}" title="https://latex.codecogs.com/svg.image?P.A.=\sqrt{\frac{V^{T}V}{s}}" />

<img src="https://latex.codecogs.com/svg.image?r_{az}" title="https://latex.codecogs.com/svg.image?r_{az}" /> and <img src="https://latex.codecogs.com/svg.image?r_{el}" title="https://latex.codecogs.com/svg.image?r_{el}" /> are the number of degree of freedom in azimuth and elevation axes respectively, and <img src="https://latex.codecogs.com/svg.image?s" title="https://latex.codecogs.com/svg.image?s" /> is the number of degree of freedom in observation data sets, which normally equals the number of observation sets minus the number of unknowns, or <img src="https://latex.codecogs.com/svg.image?s&space;=&space;\frac{m}{2}&space;-&space;n" title="https://latex.codecogs.com/svg.image?s = \frac{m}{2} - n" />. In practice the pointing accuracy of the telescope should be approximate the sum value in both azimuth and elevation axes.  

<img src="https://latex.codecogs.com/svg.image?P.A.&space;\approx&space;\sqrt{P.A._{az}^2&space;&plus;&space;P.A._{el}^2}" title="https://latex.codecogs.com/svg.image?P.A. \approx \sqrt{P.A._{az}^2 + P.A._{el}^2}" /> 

_____________________

## References

1. (2015) Charles D. Ghilani. Paul R. Wolf. Elementary Surveying An Introduction to Geomatics 14th edition. Chapter 16 Adjustments by Least Squares (page 435 - 445). ISBN 10: 1-292-06049-2  
2. (1996) A. Greve. J.-F Panis. C. Thum. The pointing of the IRAM 30-m telescope  
3. (2007) P. de Vicente. Instituto Geográfico Nacional. Deconstructing a pointing model for the 40M OAN radiotelescope
