function dataset = readcsv(filename)
% READCSV Import data to MATLAB
%   class(filename) == string
%   class(dataset) == table   

dataset = readtable(filename);

end
