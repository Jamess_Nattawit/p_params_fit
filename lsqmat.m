function [L, X, V, V1, V0, B1, B0, C, std_uw, std_Pi, n, n1, n0] = lsqmat(Az1, El1, Az0, El0, L1, L0)
%LSQMAT determine coefficients of pointing model using matrix methods
%   class(all) == double

% delAz = a + b*sec(El) + c*tan(El) - d*cos(Az)*tan(Az) + e*sin(Az)*tan(El)  --(1)
% delEl = d*sin(Az) + e*cos(Az) + f + g*sin(El) + h*cos(El)                  --(2)
% delAzCorr = dAz*cos(El)

%--------------------------------------------------------------------------

A1 = zeros(length(Az1), 8);
n1 = 0;

for c  = 1:length(Az1)
    Az = Az1(c) * (pi/180);
    El = El1(c) * (pi/180);
    
    A1(c,:) = [1 sec(El) tan(El) -cos(Az)*tan(El) sin(Az)*tan(El) 0 0 0];
    A1(c,:) = cos(El) * A1(c,:);
    
    n1 = n1 + 1;
end

A0 = zeros(length(Az0), 8);
n0 = 0;

for c  = 1:length(Az0)
    Az = Az0(c) * (pi/180);
    El = El0(c) * (pi/180);
    
    A0(c,:) = [0 0 0 sin(Az) cos(Az) 1 sin(El) cos(El)];
    
    n0 = n0 + 1;
end

% make calculation
v = 8;                      %the number of unknowns
n = n1 + n0;                %the number of observations
A = [A1; A0];               %the matrix of coefficients
L = [L1; L0];               %the matrix of observations
X = (A.' * A)\(A.' * L);    %the matrix of unknowns
B = A * X;                  %the matrix of model values
V = B - L;                  %the matrix of residuals

std_uw = sqrt((V.'* V) / (n - v));       %the std. of unit weight
C      = inv(A.'* A);                    %the cofactor matrix
std_Pi = zeros(v, 1);

for c = 1:v
    std_Pi(c) = std_uw * sqrt(C(c, c));  %the std. of the i-th adjusted unknowns 
end

% parepare for graphs
B1 = B(   1 :  n1);         %the matrix of az model values
B0 = B(n1+1 : end);         %the matrix of el model values
V1 = V(   1 :  n1);         %the matrix of az residuals
V0 = V(n1+1 : end);         %the matrix of el residuals

end

